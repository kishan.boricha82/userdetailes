package com.example.userdetaile.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.userdetaile.Bean_class.Bean_User;
import com.example.userdetaile.R;

import java.util.ArrayList;

public class Adapter_History extends RecyclerView.Adapter<Adapter_History.ViewHolder> {

    ArrayList<Bean_User> bean_histories;


    public Adapter_History(ArrayList<Bean_User> bean_historie) {
        this.bean_histories = bean_historie;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_name;
        public TextView tv_email;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_email = itemView.findViewById(R.id.tv_listemail);
            tv_name= itemView.findViewById(R.id.tv_listname);
        }
    }


    @NonNull
    @Override
    public Adapter_History.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.show_list, parent, false);

        // Return a new holder instance
        return new ViewHolder(contactView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Bean_User bean_user = bean_histories.get(position);
        TextView email = holder.tv_email;
        email.setText(bean_user.getUser_email());
        TextView name = holder.tv_name;
        name.setText(bean_user.getUser_Name());
    }

    @Override
    public int getItemCount() {
        return bean_histories.size();
    }
}
