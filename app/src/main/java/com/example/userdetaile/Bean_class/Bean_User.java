package com.example.userdetaile.Bean_class;

public class Bean_User {

    private int User_id;
    private String User_email;
    private String User_Password;
    private String User_Name;

    public int getUser_id() {
        return User_id;
    }

    public void setUser_id(int user_id) {
        User_id = user_id;
    }

    public String getUser_Name() {
        return User_Name;
    }

    public void setUser_Name(String name) {
        User_Name = name;
    }

    public String getUser_email() {
        return User_email;
    }

    public void setUser_email(String user_name) {
        User_email = user_name;
    }

    public String getUser_Password() {
        return User_Password;
    }

    public void setUser_Password(String password) {
        User_Password = password;
    }
}
