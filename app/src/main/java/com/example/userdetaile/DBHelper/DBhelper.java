package com.example.userdetaile.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.userdetaile.Constant.Constant;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;


public class DBhelper extends SQLiteAssetHelper {
    public DBhelper(Context context) {
        super(context, Constant.DBname, null, Constant.DBVersion);
    }

    public void insert(String Username, String Password,String Name) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("User_name",Name);
        cv.put("User_email", Username);
        cv.put("User_Password", Password);
        db.insert("User_data", null, cv);
    }
}
