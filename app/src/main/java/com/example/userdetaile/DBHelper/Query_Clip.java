package com.example.userdetaile.DBHelper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.userdetaile.Bean_class.Bean_User;

import java.util.ArrayList;

public class Query_Clip extends DBhelper{
    public Query_Clip(Context context) {
        super(context);
    }

    public ArrayList<Bean_User> getUser(){
        ArrayList<Bean_User> bean_user =new ArrayList<>();
        SQLiteDatabase db=this.getWritableDatabase();
        String query="Select * from User_data";
        Cursor cursor=db.rawQuery(query,null);
        cursor.moveToFirst();
        for (int i=0;i<cursor.getCount();i++){
            Bean_User beanUser=new Bean_User();
            beanUser.setUser_id(cursor.getInt(0));
            beanUser.setUser_Name(cursor.getString(1));
            beanUser.setUser_email(cursor.getString(2));
            beanUser.setUser_Password(cursor.getString(3));
            bean_user.add(beanUser);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return bean_user;
    }
}
