package com.example.userdetaile;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.userdetaile.Adapter.Adapter_History;
import com.example.userdetaile.Bean_class.Bean_User;
import com.example.userdetaile.DBHelper.Query_Clip;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private RecyclerView listView;
    ArrayList<Bean_User> bean_histories;
    Query_Clip query_clip;
    Adapter_History adapter_history;
    TextView tv_empty;
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();
        tv_empty = findViewById(R.id.tv_emptyc);
        listView = findViewById(R.id.list);
        query_clip = new Query_Clip(this);
        bean_histories = query_clip.getUser();
        if (bean_histories.isEmpty()){
            listView.setVisibility(View.GONE);
            tv_empty.setVisibility(View.VISIBLE);
        }
        else {
            LinearLayoutManager llm = new LinearLayoutManager(this);
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            listView.setLayoutManager(llm);
            adapter_history = new Adapter_History(bean_histories);
            listView.setAdapter(adapter_history);
            listView.setVisibility(View.VISIBLE);
            tv_empty.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==R.id.logout){
            loginPrefsEditor.putBoolean("saveLogin", false);
            loginPrefsEditor.apply();
            Intent intent = new Intent(HomeActivity.this,MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
        return true;
    }
}