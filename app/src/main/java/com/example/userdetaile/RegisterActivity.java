package com.example.userdetaile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.userdetaile.Bean_class.Bean_User;
import com.example.userdetaile.DBHelper.DBhelper;
import com.example.userdetaile.DBHelper.Query_Clip;

import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity {

    private EditText et_username,et_password,et_name;
    private Button btn_submit;
    private ArrayList<Bean_User> bean_users;
    private Query_Clip query_clip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        query_clip = new Query_Clip(this);
        et_username = findViewById(R.id.et_Rusername);
        et_password = findViewById(R.id.et_Rpassword);
        et_name = findViewById(R.id.et_name);
        btn_submit = findViewById(R.id.btn_submit_to);
        bean_users = query_clip.getUser();

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_username.getText().length() != 0 && et_password.getText().length() != 0 && et_name.getText().length() !=0){
                    if (!Patterns.EMAIL_ADDRESS.matcher(et_username.getText()).matches()){
                        Toast.makeText(RegisterActivity.this,"Please Enter Valid Email",Toast.LENGTH_SHORT).show();
                    }else{
                        String us = String.valueOf(et_username.getText());
                        String ps = String.valueOf(et_password.getText());
                        String na = String.valueOf(et_name.getText());
                        for (int i = 0; i < bean_users.size(); i++) {
                            if (bean_users.get(i).getUser_email().equals(us)) {
                                Toast.makeText(RegisterActivity.this,"This Username is already Taken Please Enter Another Username",Toast.LENGTH_SHORT).show();
                                break;
                            }
                            if (i==(bean_users.size() - 1)){
                                new DBhelper(RegisterActivity.this).insert(us,ps,na);
                                Intent intent = new Intent(RegisterActivity.this,MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }

                }
                else {
                    Toast.makeText(RegisterActivity.this,et_name.getText().length() == 0?"Please Enter Name":et_username.getText().length() == 0?"PLease Enter UserName":"Please Enter Password",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}