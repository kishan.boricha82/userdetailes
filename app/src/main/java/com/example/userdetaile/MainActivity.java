package com.example.userdetaile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.userdetaile.Bean_class.Bean_User;
import com.example.userdetaile.DBHelper.Query_Clip;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private EditText et_username,et_password;
    private Button btn_submit,btn_register;
    ArrayList<Bean_User> bean_users;
    Query_Clip query_clip;
    private CheckBox saveLoginCheckBox;
    private SharedPreferences loginPreferences;
    private SharedPreferences.Editor loginPrefsEditor;
    private Boolean saveLogin;
    int c=0;
    String us,ps;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        query_clip = new Query_Clip(this);
        et_username = findViewById(R.id.et_username);
        et_password = findViewById(R.id.et_password);
        btn_submit = findViewById(R.id.btn_Login);
        btn_register = findViewById(R.id.btn_Register);
        saveLoginCheckBox = findViewById(R.id.saveLoginCheckBox);
        loginPreferences = getSharedPreferences("loginPrefs", MODE_PRIVATE);
        loginPrefsEditor = loginPreferences.edit();

        saveLogin = loginPreferences.getBoolean("saveLogin", false);

        if (getIntent().getIntExtra("savelogin",0) == 1){
            saveLogin = false;
        }

        if (saveLogin) {
            int uid = loginPreferences.getInt("id",0);
            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_username.getText().length() == 0 && et_password.getText().length() == 0){
                    Toast.makeText(MainActivity.this,"Please Enter Username And Password",Toast.LENGTH_SHORT).show();
                }else if (!TextUtils.isEmpty(et_username.getText()) && !Patterns.EMAIL_ADDRESS.matcher(et_username.getText()).matches()){
                    Toast.makeText(MainActivity.this,"Please Enter Valid Email",Toast.LENGTH_SHORT).show();
                }else if(et_password.getText().length() == 0){
                    Toast.makeText(MainActivity.this,"Please Enter Password",Toast.LENGTH_SHORT).show();
                }
                else {
                    bean_users = query_clip.getUser();
                    us = String.valueOf(et_username.getText());
                    ps = String.valueOf(et_password.getText());
                    if (bean_users.isEmpty()){
                        Toast.makeText(MainActivity.this,"Please Register User",Toast.LENGTH_SHORT).show();
                    }
                    else {
                        for (int i = 0; i < bean_users.size(); i++) {
                            if (bean_users.get(i).getUser_email().equals(us)) {
                                c++;
                                if (bean_users.get(i).getUser_Password().equals(ps)) {
                                    if (saveLoginCheckBox.isChecked()) {
                                        loginPrefsEditor.putBoolean("saveLogin", true);
                                        loginPrefsEditor.putString("username", us);
                                        loginPrefsEditor.putString("password", ps);
                                        loginPrefsEditor.putString("name", ps);
                                        loginPrefsEditor.putInt("id",bean_users.get(i).getUser_id());
                                        loginPrefsEditor.apply();
                                    } else {
                                        loginPrefsEditor.clear();
                                        loginPrefsEditor.commit();
                                    }
                                    id = bean_users.get(i).getUser_id();
                                    Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }else {
                                    Toast.makeText(MainActivity.this, "Password is Incorrect", Toast.LENGTH_SHORT).show();
                                    et_username.setFocusable(true);
                                }
                            }
                        }
                        if (c==0){
                            Toast.makeText(MainActivity.this, "Username is Incorrect", Toast.LENGTH_SHORT).show();
                            et_password.setFocusable(true);
                        }
                    }
                }
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }
}